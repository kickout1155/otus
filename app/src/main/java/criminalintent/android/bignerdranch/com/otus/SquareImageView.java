package criminalintent.android.bignerdranch.com.otus;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SquareImageView extends LinearLayout {

    private ImageView imageView;
    private TextView textView;
    private static int finalWidth;
    private static int finalHeight;

    public SquareImageView(Context context) {
        super(context);
        iniUi();
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        setLayoutParams(layoutParams);
//        getChildAt(0).setLayoutParams(layoutParams);
    }

    public SquareImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        iniUi();
    }

    private void iniUi() {
        setGravity(Gravity.CENTER_HORIZONTAL);
        setGravity(Gravity.CENTER_VERTICAL);
        LayoutInflater.from(getContext()).inflate(R.layout.square_image_view, this);
        imageView = findViewById(R.id.image_view);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int myWidth = 200;
        int myHeight = 200;
        finalWidth = getSize(myWidth, widthMeasureSpec);
        finalHeight = getSize(myHeight, heightMeasureSpec);
        int min = Math.min(finalWidth, finalHeight);
        //Работает только с этой строкой, поясните пожалуйста, да это хард код (по индексу так обращаться)
        // но потратил много времени и не стал переписывать.
        getChildAt(0).measure(min,min);
        //---
        setMeasuredDimension(min,min);


    }

    private int getSize(int mySize, int measureSpec) {
        final int mode = MeasureSpec.getMode(measureSpec);
        final int size = MeasureSpec.getSize(measureSpec);
        switch (mode) {
            case MeasureSpec.EXACTLY:
                return size;
            case MeasureSpec.AT_MOST:
                if (mySize < size) {
                    return mySize;
                } else {
                    return size;
                }
            case MeasureSpec.UNSPECIFIED:
            default:
                return mySize;
        }


    }

    public static int getSquareImageViewWidth(){
        return finalWidth;
    }
    public static int getSquareImageViewHeight(){
        return finalHeight;
    }
}
