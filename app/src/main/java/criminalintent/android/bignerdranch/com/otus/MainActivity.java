package criminalintent.android.bignerdranch.com.otus;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    private SquareImageView squareImageView;
    private CircleImageView circleImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        squareImageView = findViewById(R.id.square_image_view);
        circleImageView = findViewById(R.id.circle_image_view);

    }
}
