package criminalintent.android.bignerdranch.com.otus;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Display;
import android.view.View;

public class CircleImageView extends View {
    int widthTel;
    int heightTel;
    int STEP = 2;

    int centerCircleWidth;
    int centerCircleHeight;
    float widthRadius = 4f;

    int raduis = 10;
    int width = 0;
    int height = 0;

    private Paint paint = new Paint();
    private Boolean isIniUi = false;
    Bitmap bitmap;
    public CircleImageView(Context context) {
        super(context);
    }

    private void getDisplayTelephone() {
        MainActivity ma = (MainActivity) getContext();
        Display display = ma.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        widthTel = size.x;
        heightTel = size.y;
    }

    public CircleImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int desiredWidth = 300;
        int desiredHeight = 300;

        final int measureWidth = getSize(desiredWidth, widthMeasureSpec);
        final int measureHeight = getSize(desiredHeight, heightMeasureSpec);
        setMeasuredDimension(measureWidth, measureHeight);
        getDisplayTelephone();



    }

    private int getSize(int desired, int measureSpec) {
        final int mode = MeasureSpec.getMode(measureSpec);
        final int size = MeasureSpec.getSize(measureSpec);
        switch (mode) {
            case MeasureSpec.EXACTLY:
                return size;
            case MeasureSpec.AT_MOST:
                if (desired < size) {
                    return desired;
                } else {
                    return size;
                }
            case MeasureSpec.UNSPECIFIED:
            default:
                return size;
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {

        if (!isIniUi) {
            width = getWidth();
            height = getHeight();
            int min = Math.min(width, height);
            isIniUi = true;
            centerCircleHeight = height / 2;
            centerCircleWidth = width / 2;
            bitmap = Bitmap.createBitmap(raduis*3,raduis*3, Bitmap.Config.ARGB_4444);
            bitmap.eraseColor(Color.BLUE);
            int center = bitmap.getWidth()/2;
            for (int x = 0; x < bitmap.getWidth(); x++) {
                for (int y = 0; y < bitmap.getHeight(); y++) {
                    if ((x-center)*(x-center)+(y-center)*(y-center)<=center*center) {
                        continue;
                    } else {
                        bitmap.setPixel(x,y,Color.TRANSPARENT);
                    }
                }
            }

////            (x - x0)^2 + (y - y0)^2 <= R^2
//            bitmap.setPixel(10,10,Color.TRANSPARENT);

        }

        paint.reset();
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(widthRadius);
        paint.setAntiAlias(true);
        canvas.drawBitmap(bitmap,getCenterCircleWidth(),getCenterCircleHeight(),paint);
        getWidth();
//        canvas.drawBitmap(bitmap,widthTel-SquareImageView.getSquareImageViewWidth()-raduis*2,0,paint);
        postInvalidateDelayed(500);


    }

    private float getCenterCircleHeight() {
        if (centerCircleHeight - raduis == 0) {
            centerCircleHeight = centerCircleHeight + raduis+STEP;
            return centerCircleHeight;
        }

        if (centerCircleHeight - raduis - STEP < 0) {
            centerCircleHeight = raduis;
            return centerCircleHeight;
        }

        if (centerCircleHeight + raduis == heightTel){
            centerCircleHeight = centerCircleHeight + raduis - STEP;
            return centerCircleHeight;
        }

        if (centerCircleHeight + raduis + STEP > heightTel){
//            centerCircleHeight = heightTel - raduis;
            centerCircleHeight = raduis;
            return centerCircleHeight;
        }
        centerCircleHeight = centerCircleHeight+raduis+STEP;
        return centerCircleHeight;

    }

    private float getCenterCircleWidth() {

        if (centerCircleWidth - raduis == widthTel-SquareImageView.getSquareImageViewWidth()-raduis*2) {
            centerCircleWidth = centerCircleWidth + raduis + STEP;
            return centerCircleWidth + raduis + STEP;
        }

        if (centerCircleWidth - raduis - STEP < widthTel-SquareImageView.getSquareImageViewWidth()-raduis*2) {
            centerCircleWidth = raduis;
            return centerCircleWidth;
        }

        if (centerCircleWidth + raduis == widthTel){
            centerCircleWidth = centerCircleWidth + raduis - STEP;
            return centerCircleWidth;
        }

        if (centerCircleWidth + raduis + STEP > widthTel){
//            centerCircleWidth = widthTel - raduis;
            centerCircleWidth = raduis;
            return centerCircleWidth;
        }
        centerCircleWidth = centerCircleWidth+raduis+STEP;
        return centerCircleWidth;

    }

}
